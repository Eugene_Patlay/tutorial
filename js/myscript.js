/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $("#modal").on("click", function (ev) {
        ev.preventDefault();
        $("#mydialog").dialog("open");
    });
    
    $("#delete").on("click", function (ev) {
        ev.preventDefault();
        $.ajax({
            url: "",
            type: 'DELETE',
            'success': function (responseJson)
            {
                alert(responseJson);
            }
        });
    });
});
$(document).on('submit', 'form#formCreate', function (ev) {
    ev.preventDefault();
    var serializedForm = $('form#formCreate').serializeArray();
    var serializedFormTrue = {};
    $.each(serializedForm, function (index, value) {
        serializedFormTrue[value['name']] = value['value'];
    });
    var stringtifiedForm = JSON.stringify(serializedFormTrue);
    $.ajax({
        'url': '',
        'data': stringtifiedForm,
        'type': 'POST',
        'success': function (responseJson)
        {
            $(".requestError").html(responseJson);
            //$("#mydialog").dialog("close");
        }
    });
});
$(document).on('submit', 'form#formUpdate', function (ev) {
    ev.preventDefault();
    var serializedForm = $('form#formUpdate').serializeArray();
    var serializedFormTrue = {};
    $.each(serializedForm, function (index, value) {
        serializedFormTrue[value['name']] = value['value'];
    });
    var stringtifiedForm = JSON.stringify(serializedFormTrue);
    $.ajax({
        'url': '',
        'data': stringtifiedForm,
        'type': 'PUT',
        'success': function (responseJson)
        {
            $(".requestError").html(responseJson);
            //$("#mydialog").dialog("close");
        }
    });
});

