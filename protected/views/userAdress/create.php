<?php
/* @var $this UserAdressController */
/* @var $model UserAdress */

$this->breadcrumbs=array(
	'User Adresses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List UserAdress', 'url'=>array('index')),
	array('label'=>'Manage UserAdress', 'url'=>array('admin')),
);
?>

<h1>Create UserAdress</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>