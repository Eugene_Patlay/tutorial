<?php
/* @var $this UserAdressController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'User Adresses',
);

$this->menu=array(
	array('label'=>'Create UserAdress', 'url'=>array('create')),
	array('label'=>'Manage UserAdress', 'url'=>array('admin')),
);
?>

<h1>User Adresses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));

?>
