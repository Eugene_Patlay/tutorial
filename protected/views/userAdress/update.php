<?php
/* @var $this UserAdressController */
/* @var $model UserAdress */

$this->breadcrumbs=array(
	'User Adresses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List UserAdress', 'url'=>array('index')),
	array('label'=>'Create UserAdress', 'url'=>array('create')),
	array('label'=>'View UserAdress', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage UserAdress', 'url'=>array('admin')),
);
?>

<h1>Update UserAdress <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>