<?php
/* @var $this UserAdressController */
/* @var $model UserAdress */

$this->breadcrumbs=array(
	'User Adresses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List UserAdress', 'url'=>array('index')),
	array('label'=>'Create UserAdress', 'url'=>array('create')),
	array('label'=>'Update UserAdress', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete UserAdress', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UserAdress', 'url'=>array('admin')),
);
?>

<h1>View UserAdress #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_user',
		'adress',
		'phone',
	),
)); ?>
