<?php
/* @var $this UserController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Rest',
);
echo CHtml::Link("User", $this->createUrl('rest/user'));
echo "<hr/>";
echo CHtml::Link("UsersAdres", $this->createUrl('rest/useradress'));
echo "<hr/>";
if (isset($body)) {
    $elementForm = array();
    $elementFormEmpty = true;
    echo CHtml::Link("Create", "#", array("id" => "modal"));
    echo "<hr/>";

    if ($this->isJson($body)) {
        $body = CJSON::decode($body);
    }
    echo '<div class="items">';
    if (is_array($body)) {
        foreach ($body as $elements) {
            echo '<div class = "view">';
            if (is_array($elements)) {
                foreach ($elements as $key => $element) {
                    if ($elementFormEmpty == true)
                        $elementForm[] = $key;
                    echo "<b>" . $key . "</b> " . $element . "<br/>";
                }
                $elementFormEmpty = false;
                echo CHtml::Link("Detail", $_GET['model'] . "/" . $elements["id"]);
                echo '</div>';
            }
        }
    }
    echo '</div>';
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id' => 'mydialog',
        'options' => array(
            'title' => 'Отправить данные',
            'autoOpen' => false,
            'modal' => true,
            'resizable' => false
        ),
    ));
    ?>
    <form id="formCreate">
        <?php
        if (isset($elementForm)) {
            unset($elementForm[0]);
            foreach ($elementForm as $elemForm) {
                echo CHtml::label($elemForm . ": ", $elemForm);
                echo "<br/>";
                echo CHtml::textField($elemForm);
                echo "<br/>";
            }
            echo "<br/>";
            echo CHtml::submitButton("Ok");
            echo "<hr/>";
            echo "<div class='requestError'></div>";
        }
        ?>
    </form>
    <?
    $this->endWidget('zii.widgets.jui.CJuiDialog');
}

