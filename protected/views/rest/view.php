<?php
/* @var $this UserController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Rest',
);
echo CHtml::Link("User", $this->createUrl('rest/user'));
echo "<hr/>";
echo CHtml::Link("UsersAdres", $this->createUrl('rest/useradress'));
echo "<hr/>";
if (isset($body)) {
    if ($this->isJson($body)) {
        $body = CJSON::decode($body);
    }
    echo '<div class="items">';
    echo '<div class = "view">';
    if (is_array($body)) {
        foreach ($body as $key => $element) {
            echo "<b>" . $key . "</b> " . $element . "<br/>";
        }
        $elementFormEmpty = false;
        echo CHtml::Link("Update", "#", array("id" => "modal"));
        echo "<br/>";
        echo CHtml::Link("Delete", "#", array("id" => "delete",));
        echo '</div>';
    }
    echo '</div>';
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id' => 'mydialog',
        'options' => array(
            'title' => 'Отправить данные',
            'autoOpen' => false,
            'modal' => true,
            'resizable' => false
        ),
    ));
    ?>
    <form id="formUpdate">
        <?php
        if (isset($body)) {
            unset($body["id"]);
            foreach ($body as $key => $element) {
                echo CHtml::label($key . ": ", $key);
                echo "<br/>";
                echo CHtml::textField($key, $element);
                echo "<br/>";
            }
            echo "<br/>";
            echo CHtml::submitButton("ok");
            echo "<hr/>";
            echo "<div class='requestError'></div>";
        }
        ?>
    </form>
    <?
    $this->endWidget('zii.widgets.jui.CJuiDialog');
}