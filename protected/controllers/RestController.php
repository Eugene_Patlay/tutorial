<?php

class RestController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * Список записей из модели, которая пришла к нам в адресной строке
     */
    public function actionList() {
        if (isset($_GET['model']))
            $_model = CActiveRecord::model(ucfirst($_GET['model']));
        if (isset($_model)) {
            $_data = $_model->findAll();
            if (empty($_data))
                echo sprintf('No items were found for model <b>%s</b>', $_GET['model']);
            else {
                $_rows = array();
                foreach ($_data as $_d)
                    $_rows[] = $_d->attributes;
                $body = CJSON::encode($_rows);
            }
        } else {
            echo sprintf('Error: Mode <b>list</b> is not implemented for model <b>%s</b>', $_GET['model']);
            Yii::app()->end();
        }
        $this->render("index", array('body' => $body));
    }

    /**
     * Детальное представление записи из модели по его id
     */
    public function actionView() {
        if (isset($_GET['model']))
            $_model = CActiveRecord::model(ucfirst($_GET['model']));

        if (isset($_model)) {
            $_data = $_model->findByPk($_GET['id']);
            if (empty($_data))
                echo sprintf('No items were found for model <b>%s</b>', $_GET['model']);
            else
                $this->render("view", array('body' => CJSON::encode($_data)));
        } else {
            echo sprintf('Error: Mode <b>list</b> is not implemented for model <b>%s</b>', $_GET['model']);
            Yii::app()->end();
        }
    }
    
    /**
     * Добавление записи
     */
    public function actionCreate() {
        $post = Yii::app()->request->rawBody;
        if (isset($_GET['model'])) {
            $_modelName = ucfirst($_GET['model']);
            $_model = new $_modelName;
        }
        if (isset($_model)) {
            if (!empty($post)) {
                $_data = CJSON::decode($post, true);
                if (!empty($_data)) {
                    foreach ($_data as $var => $value)
                        $_model->$var = $value;
                    if ($_model->save())
                        echo CJSON::encode($_model);
                    else {
                        // Errors occurred
                        $msg = "<h1>Error</h1>";
                        $msg .= sprintf("Couldn't create model <b>%s</b>", $_GET['model']);
                        $msg .= "<ul>";
                        foreach ($_model->errors as $attribute => $attr_errors) {
                            $msg .= "<li>Attribute: $attribute</li>";
                            $msg .= "<ul>";
                            foreach ($attr_errors as $attr_error)
                                $msg .= "<li>$attr_error</li>";
                            $msg .= "</ul>";
                        }
                        $msg .= "</ul>";
                        echo $msg;
                    }
                } else {
                    $msg = "<h1>Error</h1>";
                    $msg .= sprintf("Couldn't create model <b>%s</b>", $_GET['model']);
                    echo $msg;
                }
            }
        } else {
            echo sprintf('Error: Mode <b>create</b> is not implemented for model <b>%s</b>', $_GET['model']);
            Yii::app()->end();
        }
    }
    
    /**
     * Изменение существующей записи
     */
    public function actionUpdate() {
        $post = Yii::app()->request->rawBody;
        if (isset($_GET['model'])) {
            $_model = CActiveRecord::model(ucfirst($_GET['model']))->findByPk($_GET['id']);
            $_model->scenario = 'update';
        }
        if (isset($_model)) {
            if (!empty($post)) {
                $_data = CJSON::decode($post, true);
                foreach ($_data as $var => $value)
                    $_model->$var = $value;
                if ($_model->save()) {
                    Yii::log('API update -> ' . $post, 'info');
                    echo CJSON::encode($_model);
                } else {
                    // Errors occurred
                    $msg = "<h1>Error</h1>";
                    $msg .= sprintf("Couldn't update model <b>%s</b>", $_GET['model']);
                    $msg .= "<ul>";
                    foreach ($_model->errors as $attribute => $attr_errors) {
                        $msg .= "<li>Attribute: $attribute</li>";
                        $msg .= "<ul>";
                        foreach ($attr_errors as $attr_error)
                            $msg .= "<li>$attr_error</li>";
                        $msg .= "</ul>";
                    }
                    $msg .= "</ul>";
                    echo $msg;
                }
            } else
                Yii::log('POST data is empty');
        } else {
            echo sprintf('Error: Mode <b>update</b> is not implemented for model <b>%s</b>', $_GET['model']);
            Yii::app()->end();
        }
    }
    
    /**
     * Удаление записи
     */
    public function actionDelete() {
        if (isset($_GET['model']))
            $_model = CActiveRecord::model(ucfirst($_GET['model']));

        if (isset($_model)) {
            $_data = $_model->findByPk($_GET['id']);

            if (!empty($_data)) {
                $num = $_data->delete();

                if ($num > 0)
                    echo "Rows delete"; //this is the only way to work with backbone
                else
                    echo sprintf("Error: Couldn't delete model <b>%s</b> with ID <b>%s</b>.", $_GET['model'], $_GET['id']);
            } else
                echo sprintf("Error: Didn't find any model <b>%s</b> with ID <b>%s</b>.", $_GET['model'], $_GET['id']);
        } else {
            echo sprintf('Error: Mode <b>delete</b> is not implemented for model <b>%s</b>', ucfirst($_GET['model']));
            Yii::app()->end();
        }
    }

    /**
     * Позволяет определить является ли строка Json'ном
     * @param $string
     * @return bool
     */
    public function isJson($string) {
        CJSON::decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    public function actionIndex() {
        $this->render('index');
    }

}
