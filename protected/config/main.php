<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'TestRestApi',
    'language' => 'ru',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
    ),
    'modules' => array(
        // uncomment the following to enable the Gii tool
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => '1111',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
    ),
    // application components
    'components' => array(
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(
            'urlFormat' => 'path',
            //'showScriptName'=>false,
            'rules' => array(
                // REST patterns
                array('rest/list', 'pattern' => 'rest/<model:\w+>', 'verb' => 'GET'),
                array('rest/view', 'pattern' => 'rest/<model:\w+>/<id:\d+>', 'verb' => 'GET'),
                array('rest/update', 'pattern' => 'rest/<model:\w+>/<id:\d+>', 'verb' => 'PUT'),
                array('rest/delete', 'pattern' => 'rest/<model:\w+>/<id:\d+>', 'verb' => 'DELETE'),
                array('rest/create', 'pattern' => 'rest/<model:\w+>', 'verb' => 'POST'),
                // Other rules
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                //Custom rules
                '<controller1:\w+>/<id:\d+>/<controller2:\w+>' => '<controller2>/viewByUser',
                '<controller1:\w+>/<id1:\d+>/<controller2:\w+>/<id:\d+>' => '<controller2>/view',
                '<controller1:\w+>/<action1:\w+>/<id1:\d+>/<controller2:\w+>/<action2:\w+>/<id:\d+>' => '<controller2>/<action2>',
                '<controller1:\w+>/<action1:\w+>/<controller2:\w+>/<action2:\w+>' => '<controller2>/<action2>',
            ),
        ),
        /*
          'db'=>array(
          'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
          ),
         */
        // uncomment the following to use a MySQL database
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=testrestapi',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                // uncomment the following to show log messages on web pages
//                array(
//                    'class' => 'CWebLogRoute',
//                ),
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'webmaster@example.com',
    ),
);
